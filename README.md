# README #

General RNA-seq pipeline made for getting single-cell RNA and multi-cell RNA data as close as possible with a certain combination of commands.

PAPER; ```Theme 9 - Single cell sequencing analysis Casper Peters & Robert Warmerdam 341942 346462 upload2.zip```

# LICENSE

Copyright (c) [2018] [Casper Peters and Robert Warmerdam] under the MIT license.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Snakemake variants ##
Recently, snakemake variants have been made by both @Berghopper and @ca_warmerdam . 

* @Berghopper 's variant can be found [here.](https://bitbucket.org/Berghopper/snakemakerepoth11/src/e9b06cb954750d94f23e4530f95909e8b5694717/Final/?at=master)
* @ca_warmerdam 's variant can be found [here.](https://bitbucket.org/ca_warmerdam/data_processing_2018/src/11a2a15d133ab2edbfa3429cf3bdc6f0fda91868/Final_Assignment/?at=master)