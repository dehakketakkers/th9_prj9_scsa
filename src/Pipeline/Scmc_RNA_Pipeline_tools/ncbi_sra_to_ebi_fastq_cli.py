#!/usr/bin/env python3
"""
This python script will download .fastq/.fq files from the EBI (http://www.ebi.ac.uk/) using any ftp link of the NCBI
(https://www.ncbi.nlm.nih.gov/) containing .sra (Sequence Read Archives file) files. This is, to prevent the need of
using the poorly documented fastq-dump (https://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?view=toolkit_doc&f=fastq-dump)
command line tool and understanding all it's parameters.
"""
__author__ = "Casper Peters & Robert Warmerdam (Berghopper & ca_warmerdam)"
__credits__ = ["Casper Peters", "Robert Warmerdam"]
__version__ = "0.2.1"
__maintainer__ = "de hakketakkers (Bitbucket team)"
__status__ = "Development"

# imports
import sys
import os
import argparse
import gzip
from ftplib import FTP
# constants
# classes
# functions
def input_parsing():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--url", type=test_ncbipath, required=True, help="Ftp url for NCBI that contains the .sra files. This can "
                                                           "also be a bunch of folders, the script will look within "
                                                           "these for the .sra files.")
    parser.add_argument('--output', type=test_filepath, required=False, help="Give the output folder to which to save the fastq "
                                                               "files. If not given, output folder will be created "
                                                                   "at run location.")
    parser.add_argument('--no_decompress', required=False, action="store_true", help="For leaving downloaded files "
                                                                                     "compressed.")
    parser.add_argument('--nodel_gz', required=False, action="store_true", help="Omits deleting the .fastq.gz"
                                                                                        "'s after decompression.")
    parser.add_argument('--no_queries', required=False, action="store_true", help="For omitting querries before "
                                                                                 "downloading")
    parser.add_argument('--non_verbose', required=False, action="store_true", help="For omitting general output of "
                                                                                   "program.")
    parser.add_argument('--less_verbose', required=False, action="store_true", help="For omitting most output of "
                                                                                    "program.")

    args = parser.parse_args()
    return args

def test_ncbipath(ncbipath):
    '''
    Tests if ftp url for NCBI is genuine and reachable.
    '''
    # Remove "ftp://" if present.
    if "//" in ncbipath:
        ncbipath = ncbipath.split("//", 1)[1]

    # Test if ftp url for NCBI is genuine.
    if not ("/" in ncbipath and "ftp-trace.ncbi.nlm.nih.gov" == ncbipath.split("/")[0]):
        raise argparse.ArgumentTypeError("Ftp url for NCBI '{}' is not genuine.".format(ncbipath))
    else:
        pass

    ftp_url_head = ncbipath.split("/")[0]
    # Test if ftp url for NCBI is reachable.
    try:
        ftp = FTP(ftp_url_head)
        ftp.login()
        try:
            ftp.quit()
            ftp.close()
        except:
            pass
    except:
        raise argparse.ArgumentTypeError("Ftp url for NCBI '{}' is not reachable.".format(ncbipath))

    return ncbipath

def test_filepath(filepath):
    '''Tests if filepath does exist.'''
    if not os.path.isdir(filepath):
        raise argparse.ArgumentTypeError("filepath '{}' does not exist.".format(filepath))
    else:
        pass
    return filepath

def collect_sra_filenames(argv):
    '''
    Collects sra_filenames from the NCBI ftp server. This uses a recursive function.
    This function mainly does the printing and some fancy url splitting.
    if URL is a direct link to a file, build EBI link directly from it.
    '''
    ftp_url = argv.url
    ftp_url_head = ftp_url.split("/")[0]
    ftp_url_tail = "/"+"/".join(ftp_url.split("/")[1:])
    is_file = False
    # check for non verbosity, if not there, go ahead and print!
    if not argv.non_verbose:
        print('Connecting to: ', ftp_url_head)
        print('Selected file/directory to scan (from): ', ftp_url_tail)
    # check if url tail is a file or not.
    if ftp_url_tail.endswith('.sra'):
        is_file = True
    # if it is not a file, handle underlying directories.
    if not is_file:
        try:
            sra_files = recursive_sra_filename_collector(argv, ftp_url_head, ftp_url_tail)
        except:
            sys.exit("Something went wrong while trying to scan files on the given directory. Are you connected to the "
                     "internet or is the url wrong? Given directory to scan from: "+ftp_url_tail)
    else:
        # if it is a file, collect it's size
        try:
            sra_files = sra_filename_info(argv, ftp_url_head, ftp_url_tail)
        except:
            sys.exit("Something went wrong while trying to scan the given file. Are you connected to the internet or is"
                     " the url wrong? Given file to scan: "+ftp_url_tail)
    # check for non verbosity, if not there, go ahead and print!
    if not argv.non_verbose:
        print(str(len(sra_files))+' NCBI .sra File(s) have been found, totalling '+
              str(round(sum([int(sra_file[1]) for sra_file in sra_files])/1073741824, 2))+' GB')

    return sra_files

def recursive_sra_filename_collector(argv, ftp_url_head, ftp_url_directory):
    '''
    Recursive function to collect all .sra filenames on the NCBI ftp server.
    Returns list of tuples wherin;
    [(item_name, item_size),(item_name, item_size)]
    '''
    sra_files = list()
    # login to NCBI server
    ftp = FTP(ftp_url_head)
    ftp.login()
    # ask for general file and folder info and for each item, append to file list is for files, go deeper for folders
    for item in ftp.mlsd(ftp_url_directory, facts=["type", "size"]):
        item_name = item[0]
        item_type = item[1]['type']
        item_size = item[1]['size']
        # omit parent and child directory
        if item_type == 'pdir' or item_type == 'cdir':
            continue
        # otherwise, if item is a file, add to file list.
        elif item_type == 'file' and item_name.endswith('.sra'):
            # check for less verbosity, if not present, print ahead!
            if not argv.less_verbose:
                print('NCBI file found!; name=' + item_name + ' size=' + str(int(int(item_size) / 1048576)) + ' MB')
            sra_files.append((item_name, item_size))
        # otherwise, if item is a folder, go deeper until files are found
        elif item_type == 'dir':
             sra_files.extend(recursive_sra_filename_collector(argv, ftp_url_head, ftp_url_directory+'/'+item_name))
        else:
            # check for less verbosity, if not present, print ahead!
            if not argv.less_verbose:
                print('what in the name of Bio-informatics is this? File type not recognized, skipping...')
            continue
    try:
        ftp.quit()
        ftp.close()
    except:
        pass
    return sra_files

def sra_filename_info(argv, ftp_url_head, ftp_url_file):
    '''
    Asks size of file on ftp server. and returns it together with name
    '''
    ftp_file_name = ftp_url_file.split('/')[-1]
    ftp = FTP(ftp_url_head)
    ftp.login()
    sra_file = (ftp_file_name, ftp.size(ftp_url_file))
    # check for less verbosity, if not present, print ahead!
    if not argv.less_verbose:
        print('NCBI file found!; name=' + sra_file[0] + ' size=' + str(int(sra_file[1] / 1048576)) + 'MB')
    return [sra_file]

def build_fastq_links(sra_files):
    '''
    Loops through all ncbi .sra links, and converts these using the build_link function.
    This will return a list of fastq links that are on the EBI.
    '''
    fastq_links = list()
    try:
        for sra_file in sra_files:
            fastq_links.append(build_link(sra_file[0]))
    except:
        sys.exit("Something went wrong while trying to build EBI links, is/are your filename(s) incorrect?")
    return fastq_links

def build_link(filename):
    '''
    Builds a link corresponding on the name of an sra file.
    Documentation of this process can be found here: http://www.ebi.ac.uk/ena/browse/read-download
    '''
    baseLink = "ftp.sra.ebi.ac.uk/vol1/fastq/{0}/{1}{2}/{2}.fastq.gz"
    filename = filename.split(".")[0]
    mainDir = filename[:6]
    no_digits = len(''.join(c for c in filename if c.isdigit()))
    # check how many digits there are, and build corresponding subDir
    if no_digits == 6:
        subDir = ""
    elif no_digits == 7:
        subDir = "00" + filename[-1] + '/'
    elif no_digits == 8:
        subDir = "0" + filename[-2] + '/'
    elif no_digits == 9:
        subDir = filename[-3] + '/'

    return baseLink.format(mainDir, subDir, filename)

def fastq_info(fastq_links, argv):
    '''
    queries some info and asks whether or not user would like to proceed download.
    '''
    fastq_size = list()
    ftp = FTP("ftp.sra.ebi.ac.uk")
    ftp.login()
    # for each link, query information
    for fastq_link in fastq_links:
        fastq_file_directory = '/'+'/'.join(fastq_link.split('/')[1:])
        fastq_size.append(ftp.size(fastq_file_directory))

    fastq_amount_size = (str(len(fastq_size)) , str(round(sum(fastq_size) / 1073741824, 2))+' GB')
    # check for non verbosity
    if not argv.non_verbose:
        print("Found "+fastq_amount_size[0]+' EBI .fast.gz files, totalling '+fastq_amount_size[1]+'.')
    dl_bool = fastq_download_query(fastq_amount_size, argv)

    # check for non verbosity
    if not argv.non_verbose and dl_bool:
        print("Downloading "+fastq_amount_size[0]+' files, totalling '+fastq_amount_size[1]+'.')
    if not dl_bool and not argv.non_verbose:
        print("Cancelled download(s), terminating program.")
    # close connection
    try:
        ftp.quit()
        ftp.close()
    except:
        pass
    return dl_bool, fastq_size

def fastq_download_query(fastq_amount_size, argv):
    '''
    Download query for user.
    '''
    # check noqueries argument.
    if not argv.no_queries:
        dl_inp = input("Do you want to download "+fastq_amount_size[0]+' files, totalling '+fastq_amount_size[1]+
                       '? [y/n]: ')
        if dl_inp in 'Yy':
            return True
        else:
            return False
    else:
        return True

def fastq_download(fastq_links, fastq_size, argv):
    '''
    Downloads all .fastq.gz given the links.
    '''
    output_folder = argv.output
    # check if output folder is given, if not make one.
    if not os.path.exists(argv.output):
        os.mkdir(argv.output)
    # change to output folder
    os.chdir(output_folder)
    # check for non_verbosity
    if not argv.non_verbose:
        print("All files will be downloaded to: ", os.getcwd())
    # open ftp connection and accuire all links
    ftp = FTP("ftp.sra.ebi.ac.uk")
    ftp.login()
    for fastq_index, fastq_link in enumerate(fastq_links):
        fastq_file_directory = '/'+'/'.join(fastq_link.split('/')[1:])
        fastq_file_name = fastq_link.split('/')[-1]
        # check for less verbosity, if not print!
        if not argv.less_verbose:
            print('Starting download: '+fastq_file_name+', size: '+ str(int(fastq_size[fastq_index] / 1048576)) +
                  ' MB,' + ' item ['+str(fastq_index+1)+'/'+str(len(fastq_links))+'].')
        # make new binary file
        with open(fastq_file_name, 'w+b') as fastq_file:
            ftp.retrbinary('RETR '+fastq_file_directory, fastq_file.write)
            # close file
            fastq_file.close()
    # close connection
    try:
        ftp.quit()
        ftp.close()
    except:
        pass
    # check for non verbosity
    if not argv.non_verbose:
        print("Finished downloading!")
    return 0

def fastq_unpack_query(argv):
    '''
    Asks whether to unpack or not, if 'argv.noqueries' is False.
    '''

    gz_files = [file for file in os.listdir(os.getcwd()) if file.endswith(".fastq.gz")]
    gz_amount = str(len(gz_files))

    # check noqueries argument.
    if not argv.no_queries:
        up_inp = input("Do you want to unpack "+ gz_amount+" files?" + " Make sure to have enough space available!"
                       ' [y/n]: ')
        if up_inp in 'Yy':
            return True
        else:
            return False
    else:
        return True

def fastq_unpack(argv):
    '''
    Unpacks fastq.gz files in a given 'argv.output' directory.
    '''
    # check for non_verbose, if not True, print!
    if not argv.non_verbose:
        print("Starting with unpacking!")
    for filename in os.listdir(os.getcwd()):
        if filename.endswith(".fastq.gz"):
            # produce new filename without '.gz'
            outfilename = ".".join(filename.split(".")[:-1])
            if not argv.less_verbose:
                print("Unpacking: "+filename)
            with gzip.open(filename, 'rb') as f:
                with open(outfilename, 'wb') as unpacked:
                    unpacked.write( f.read() )
            # check for nodel_gz, if not true, delete gz file.
            if not argv.nodel_gz:
                os.remove(filename)
    # check for non_verbose, if not True, print!
    if not argv.non_verbose:
        print("Finished unpacking!")
    return 0

def main(argv=None):
    '''
    Initiates script.
    '''
    if argv is None:
        argv = sys.argv[1:]

    # work
    # check if non_verbose, if so, turn on less verbose too.
    if argv.non_verbose:
        argv.less_verbose = True
    # check if there is output, if not, specify standard.
    if not argv.output:
        argv.output = 'ncbi_sra_to_ebi_fastq_cli_OUTPUT'
    sra_files = collect_sra_filenames(argv)
    # for each sra_file, query the fastq equivalent
    fastq_links = build_fastq_links(sra_files)
    # query for download
    dl_bool, fastq_size = fastq_info(fastq_links, argv)
    if dl_bool:
        fastq_download(fastq_links, fastq_size, argv)
    else:
        sys.exit()

    # decompress fastq files after download if no_decompress is false
    if not argv.no_decompress:
        if fastq_unpack_query(argv):
            fastq_unpack(argv)

    return 0

# initiate
if __name__ == "__main__":
    sys.exit(main(argv=input_parsing()))