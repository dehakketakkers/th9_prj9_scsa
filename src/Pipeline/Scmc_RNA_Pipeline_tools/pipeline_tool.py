#!/usr/bin/env python3
"""
This subpackage contains a basic tool class. 
"""
__author__ = "Casper Peters & Robert Warmerdam (Berghopper & ca_warmerdam)"
__credits__ = ["Casper Peters", "Robert Warmerdam"]
__maintainer__ = "de hakketakkers (Bitbucket team)"
__status__ = "Prototype"

# imports
import sys
# constants
# classes


class PipelineTool():
    def __init__(self, log_file, executable):
        self.log_file = log_file
        self.executable = executable
# functions


def main(argv=None):
    """
    initiates program
    """
    if argv is None:
        argv = sys.argv
    # work
    return 0


# initiate
if __name__ == "__main__":
    sys.exit(main())
